integer main(){
	integer status;
	integer fileDescriptor;
	string wordRead;
	integer newLseek;
	string wordToWrite;

	fileDescriptor = 0;
	newLseek = 1;

	status = Write(fileDescriptor, "a");
	print("Writing");
	print("Returned");
	print(status);

	status = Read(fileDescriptor, wordRead);
	print("Reading");
	print("Returned");
	print(status);

	status = Seek(fileDescriptor, newLseek);
	print("Seeking");
	print("Returned");
	print(status);

	return 0;
}
