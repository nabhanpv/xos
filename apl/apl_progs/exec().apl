decl
	integer oddsUpTo(integer n);
enddecl

integer oddsUpTo(integer n)
{
	integer i;
	i = 1;

	while (i < n) do		
		print(i);
		i = i + 2;
	endwhile;
	
	return 0;
}


integer main() {
	integer status;
	integer pid;
	print("Before exec");
	pid = Fork();
	breakpoint;
	if(pid == -1) then
		print("Fork failed");
	endif;
	if(pid == -2) then
		status = Exec("even.xsm");
	else
		status = oddsUpTo(20);
	endif;
	breakpoint;
	return 0;
}
