decl
	integer evensUpTo(integer n);
enddecl

integer evensUpTo(integer n)
{
	integer i;
	i = 0;
	
	while (i < n) do
		//print("Even");
		print(i);
		i = i + 2;
	endwhile;

	return 0;
}

integer main()
{
	integer a;
	
	a = evensUpTo(20);

	return 0;
}
