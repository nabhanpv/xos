integer main(){
	integer status;
	integer fileDescriptor;
	string fileName;
	string wordRead;

	fileName = "file1.dat";

	status = Create(fileName);
	print(status);
	print("");

	// open a empty file and try to read from it
	fileDescriptor = Open(fileName);
	print("Opening");
	print(fileName);
	print("");

	print("Returned");
	print(fileDescriptor);
	print("");

	status = Read(fileDescriptor, wordRead);
	print("Reading");
	print(fileName);
	print("");

	print("Returned");
	print(status);
	print("");

	return 0;
}
