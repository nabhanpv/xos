integer main(){
	integer pid;
	print("Before fork");
	pid = Fork();
	if(pid == -1) then
		print("Fork failed");
	endif;
	
	print("After Fork");
	
	return 0;
}
